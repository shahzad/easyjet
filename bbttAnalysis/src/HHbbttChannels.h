#ifndef BBTTANALYSIS_CHANNELS
#define BBTTANALYSIS_CHANNELS

namespace HHBBTT
{

  enum Channel
  {
    LepHad = 0,
    HadHad = 1,
    LepHad1B = 2,
    HadHad1B = 3,
    ZCR = 4,
    TopEMuCR = 5,
  };

    enum TriggerChannel
  {
    SLT,
    LTT,
    ETT,
    ETT_4J12,
    MTT_2016,
    MTT_high,
    MTT_low,
    STT,
    DTT,
    DTT_2016,
    DTT_4J12,
    DTT_L1Topo,
  };

}

#endif
